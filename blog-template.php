<?php
/*
Template Name: Blog Page
*/
?>
<?php get_header(); ?>

<main id="page-content">
	<?php if(have_posts()): while(have_posts()): the_post(); ?>
		<h1 class="col-sm-12" id="blog-page-header"><?php the_title(); ?></h1>
	<?php endwhile; endif; ?>
	<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
	<?php $post_args = array(
		'post_type' => 'post',
		'post_status' => 'publish',
		'paged' => $paged
	); ?>

	<?php
        	global $wp_query;
        	$temp = $wp_query;  // assign orginal query to temp variable for later use
        	$wp_query = null;
        	$wp_query = new WP_Query($post_args);
       	?>

		<?php if($wp_query->have_posts()): while($wp_query->have_posts()): $wp_query->the_post(); ?>
			<section>
					<?php $thumbnail_args = array(
						'class' => 'img-responsive'
					); ?>
					<a href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail('post-thumbnail', $thumbnail_args); ?>
					</a>
					<a href="<?php the_permalink(); ?>">
						<h2><?php the_title(); ?></h2>
					</a>
					<time datetime="<?php the_time('Y-m-d'); ?>"><?php the_time('F j, Y'); ?></time>
					<address class="author">Author: <?php the_author(); ?></address>
					<blockquote><?php the_excerpt(); ?></blockquote>
					<a href="<?php the_permalink(); ?>">Read More</a>
			</section>
		<?php endwhile; ?>

		<nav>
                	<?php previous_posts_link('&laquo;&nbsp;Newer&nbsp;Posts'); ?>
                	<?php next_posts_link('Older&nbsp;Posts&nbsp;&raquo;'); ?>
        	</nav>

	<?php endif; ?>
	<?php $wp_query = $temp; ?>
	</div>
</main>

<?php get_footer(); ?>
