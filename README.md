# WordPress Bootstrap Fundamental Theme #
** Created by Thought Space Designs **

-----

This theme was created for use as a starter theme. Simply clone the repo into your "themes" directory, rename the folder and the actual theme by editing the style.css file, and then activate using the administration area. This theme is meant for hacking, and should not be used as a parent theme. Simply download and start creating awesomeness.

### Included Scripts ###

* jQuery v1.10.2
* Bootstrap v3.0.3

### Included Functionality ###

* Custom color intensity modification function for toning colors up and down.
* Custom hex2rgb function for converting colors for use in RGBA.
* ".col-sm-align-middle" and ".col-sm-align-bottom" classes for aligning columns within Bootstrap rows.
* universal.php file which displays both on the front end and in the WordPress editor. Use this for generic styles (h1, h2, p, em, etc).

### How do I get set up? ###

** The "easy" way **

* Install WordPress
* [Download the repository](https://bitbucket.org/thoughtspacedesigns/wordpress-bootstrap-fundamental-theme/downloads)
* Change ZIP name to desired theme name
* Navigate to "Appearance > Themes" in your WordPress, click "Add New", and then click "Upload Theme".
* Select your ZIP and upload.
* Change the theme name inside of the style.css template.
* Activate theme from admin.

** The BASH way **

* Install WordPress
* SSH the server and cd into your themes directory
* git clone https://yourUsername@bitbucket.org/thoughtspacedesigns/wordpress-bootstrap-fundamental-theme.git desiredThemeFolderName
* Change the theme name inside of the style.css template.
* Activate theme from admin.

### For questions and concerns ###

* We can be reached for support at contact@thoughtspacedesigns.com