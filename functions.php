<?php


/**
 * 
 * PHP Includes
 * 
 * The files below all contain code that deals with a specific piece of functionality for the theme
 * Files are organized by functionality and sorted by name.
 * For example, if you're looking for funtions related to color adjustment, the file is probably named color.php
 * 
 * The "core" folder contains any files that were written by Thought Space Designs for the purposes of this starter theme
 * The "lib" folder contains any required PHP scripts or plugins for the functionality of this theme (dependencies)
 * Please make sure that any jQuery or Javascript libraries are added to the JS folder, not the lib folder. Lib folder is for PHP scripts (and their dependencies) only.
 * 
**/


/**
 * PLEASE MAKE SURE TO USE ALPHABETICAL ORDERING FOR ANY INCLUDES BELOW
**/


    //PHP Dependencies below. These are external PHP scripts or plugins required for theme functionality
        include 'functions/lib/color.php';

    //Custom PHP scripts below. These are scripts written by Thought Space for the purposes of this starter theme

        //The files below load all scripts and styles required by the theme. In order to load any stylesheets or jQuery plugins, visit the below files
        include 'functions/core/scripts.php';
        include 'functions/core/styles.php';
    
        //These are all other custom files for the theme, named appropriately.
        include 'functions/core/menus.php';
        include 'functions/core/widgets.php';
        





?>
