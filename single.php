<?php get_header(); ?>

	<main id="page-content">

		<?php if(have_posts()): while(have_posts()): the_post(); ?>

			<h1><?php the_title(); ?></h1>			

			<?php the_content(); ?>

		<?php endwhile; endif; ?>

		<?php comments_template(); ?>

	
	</main>

<?php get_footer(); ?>
