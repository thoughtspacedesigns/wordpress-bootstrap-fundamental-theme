<section id="post-a-comment">
	<?php
                function comment_fields($fields) {

                        $commenter = wp_get_current_commenter();
                        $req = get_option( 'require_name_email' );
                        $aria_req = ( $req ? " aria-required='true'" : '' );

                        $fields[ 'author' ] = '<p class="comment-form-author col-sm-6">'.
                                '<label for="author">' . __( 'Name' ) . '</label>'.
                                ( $req ? '<span class="required">*</span>' : '' ).
                                '<input id="author" name="author" type="text" value="'. esc_attr( $commenter['comment_author'] ) .
                                '" size="30" tabindex="1"' . $aria_req . ' /></p>';

                        $fields[ 'email' ] = '<p class="comment-form-email col-sm-6">'.
                                '<label for="email">' . __( 'Email' ) . '</label>'.
                                ( $req ? '<span class="required">*</span>' : '' ).
                                '<input id="email" name="email" type="text" value="'. esc_attr( $commenter['comment_author_email'] ) .
                                '" size="30"  tabindex="2"' . $aria_req . ' /></p>';

                        return $fields;
                }
                add_filter('comment_form_default_fields','comment_fields');


                $comment_form_args = array(
                        'fields' => apply_filters( 'comment_form_default_fields', $fields ),
                        'title_reply'       => __( 'Leave a Reply' ),
  			'title_reply_to'    => __( 'Leave a Reply to %s' ),
			'comment_notes_before' => '',
                        'comment_notes_after' => '',
                        'comment_field' => '<p class="comment-form-comment col-sm-12"><label for="comment">' . _x( 'Comment', 'noun' ) . '</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>'
                ); ?>
                <?php comment_form($comment_form_args); ?>
</section>
<section id="comments">
	<?php if ( have_comments() ) : ?>
                        <h3 class="comments-title">Comments</h3>
                        <ul class="comment-list">
                                <?php
                                        function custom_comment_template($comment, $args, $depth){
                                                $GLOBALS['comment'] = $comment; ?>
                                                <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
                                                        <div id="comment-<?php comment_ID(); ?>">
                                                                <div class="comment-author vcard">
                                                                        <?php printf(__('<cite class="fn">%s</cite>'), get_comment_author_link()) ?> - <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('[Edit]'),'  ','') ?> - <?php comment_reply_link(array_merge( array('reply_text'=>'[Reply]'), array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?></div>
                                                                </div>
                                                                <?php if ($comment->comment_approved == '0') : ?>
                                                                        <em><?php _e('Your comment is awaiting moderation.') ?></em>
                                                                        <br />
                                                                <?php endif; ?>

                                                                <?php comment_text() ?>

                                                        </div>
                                                </li>
                                <?php
                                        }

                                        wp_list_comments( array(
                                                'style'      => 'ul',
                                                'avatar_size' => 24,
                                                'callback' => 'custom_comment_template'
                                        ) );
                                ?>
                        </ul><!-- .comment-list -->
                <?php else: ?>
                        <h1 class="comments-title">No Comments Yet.</h1>
                        <p>Be the first to leave a comment on this article!</p>
                <?php endif; ?>
</section>
