<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <title><?php single_post_title(); ?> - <?php bloginfo('description'); ?> - <?php bloginfo('name'); ?></title>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

        <link rel="icon" type="image/png" href="<?php bloginfo("template_url"); ?>/images/favicon.png" />

        <?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
        <?php
            wp_nav_menu( array(
                'container'     => 'nav',
                'container_class' => 'header-nav',
                'depth'         => 3,
                'theme_location' => 'header-menu'
            ));
        ?>
