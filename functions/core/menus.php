<?php

defined('ABSPATH') or die("You shouldn't be accessing this file directly. ");

/**
 * Register Nav Menus
 * You can add more menus below by adding to the array
**/


function bstarter_register_nav_menus() {
    register_nav_menus(array(
        'header-menu' => __('Header Navigation')
    ));
}

add_action('init', 'bstarter_register_nav_menus');

?>