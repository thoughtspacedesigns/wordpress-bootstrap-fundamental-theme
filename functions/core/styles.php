<?php

defined('ABSPATH') or die("You shouldn't be accessing this file directly. ");

/**
 * Load required CSS files
**/



function bstarter_load_frontend_styles(){
    //Set a variable for the parent theme directory
    $parent_theme_dir = get_template_directory_uri();
	
	//Register all of our stylesheets required by the theme
	wp_register_style('reset', $parent_theme_dir.'/css/reset-min.css');
	wp_register_style('bootstrap', $parent_theme_dir.'/css/bootstrap.min.css', array('reset'));
	wp_register_style('bootstrap-theme', $parent_theme_dir.'/css/bootstrap-theme.min.css', array('reset', 'bootstrap'));
	wp_register_style('theme-css', get_bloginfo('stylesheet_url'), array('reset', 'bootstrap'));
	wp_register_style('universal-php', $parent_theme_dir.'/css/universal.php', array('reset', 'bootstrap', 'theme-css'));
	wp_register_style('style-php', $parent_theme_dir.'/css/style.php', array('reset', 'bootstrap', 'theme-css', 'universal-php'));
	wp_register_style('tablet-php', $parent_theme_dir.'/css/tablet.php', array('reset', 'bootstrap', 'theme-css', 'universal-php', 'style-php'));
	wp_register_style('mobile-php', $parent_theme_dir.'/css/mobile.php', array('reset', 'bootstrap', 'theme-css', 'universal-php', 'tablet-php', 'style-php'));
	
	//Load the styles up on the site
	wp_enqueue_style('reset');
	wp_enqueue_style('bootstrap');
	wp_enqueue_style('bootstrap-theme');
	wp_enqueue_style('theme-css');
	wp_enqueue_style('universal-php');
	wp_enqueue_style('style-php');
	wp_enqueue_style('tablet-php');
	wp_enqueue_style('mobile-php');
}

add_action( 'wp_enqueue_scripts', 'bstarter_load_frontend_styles' );


/**
 * Load up any stylesheets for the WordPress editor
**/



function bstarter_custom_editor_styles() {
    add_editor_style( 'css/universal.php' );
}
add_action( 'admin_init', 'bstarter_custom_editor_styles' );



?>