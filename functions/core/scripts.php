<?php

defined('ABSPATH') or die("You shouldn't be accessing this file directly.");

/**
 * Load required javascript files
**/

function bstarter_load_frontend_scripts() {
    //Set variable for the parent theme directory
    $parent_theme_dir = get_template_directory_uri();
    
    //Register our required theme scripts
    wp_register_script('bootstrap', $parent_theme_dir.'/js/bootstrap.min.js', array('jquery'));
    wp_register_script('sitejquery', $parent_theme_dir.'/js/sitejquery.js', array('jquery'));
    
    //Load the scripts up on the site
    wp_enqueue_script('bootstrap');
    wp_enqueue_script('sitejquery');
    
}

add_action( 'wp_enqueue_scripts', 'bstarter_load_frontend_scripts' );


?>