<?php
header("Content-type: text/css");
$absolute_path = explode('wp-content', $_SERVER['SCRIPT_FILENAME']);
$wp_load = $absolute_path[0] . 'wp-load.php';
require_once($wp_load);
?>

@charset "utf-8";
/* CSS Document */


@font-face {
    font-family: 'YOUR-FONT';
    src: url('../fonts/your-font.eot');
    src: url('../fonts/your-font.eot?#iefix') format('embedded-opentype'),
         url("../fonts/your-font.woff") format("woff"),
         url('../fonts/your-font.ttf') format('truetype'),
         url('../fonts/your-font.svg#font') format('svg');
}
