<?php
header("Content-type: text/css");
$absolute_path = explode('wp-content', $_SERVER['SCRIPT_FILENAME']);
$wp_load = $absolute_path[0] . 'wp-load.php';
require_once($wp_load);
?>

@charset "utf-8";
/* CSS Document */


body:before
{
content: "";
position: fixed;
top: -10px;
left: 0px;
width: 100%;
height: 10px;
z-index: 100;
-webkit-box-shadow: 0px 1px 10px #333;
-moz-box-shadow: 0px 1px 10px #333;
box-shadow: 0px 1px 10px #333;
z-index:10000;
}

.col-sm-align-bottom,
.col-sm-align-middle{
display:table;
table-layout:fixed;
width:100% !important;
padding-left:15px;
padding-right:15px;
box-sizing:content-box;
}

.col-sm-align-bottom *[class*='col-sm-']{
display:table-cell;
vertical-align:bottom;
float:none;
table-layout:fixed;
}

.col-sm-align-middle *[class*='col-sm-']{
display:table-cell;
vertical-align:middle;
float:none;
}
