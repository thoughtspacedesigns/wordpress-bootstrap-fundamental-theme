<?php
header("Content-type: text/css");
$absolute_path = explode('wp-content', $_SERVER['SCRIPT_FILENAME']);
$wp_load = $absolute_path[0] . 'wp-load.php';
require_once($wp_load);
?>

@charset "utf-8";
/* CSS Document */




@media (max-width: 767px) {

.col-sm-align-bottom,
.col-sm-align-middle{
display:block;
}

.col-sm-align-bottom *[class*='col-sm-'],
.col-sm-align-middle *[class*='col-sm-']{
display:block;
float:left;
width:100%;
}


}
